# Mi configuración de Emacs

Una buena parte del código es tomado de la configuración del profesor [Mike Zamansky](https://github.com/zamansky/using-emacs) con algunos ligeros cambios.

La organización (y algo de código) fueron tomados de la configuración de [Aaron Bedra](http://abedra.github.com/emacs.d).

## Instalación

```bash
 git clone https://gitlab.com/mickie1/dot-emacs.git ~/.emacs.d

```
